package range;

import java.util.Iterator;

public class RangeRevIterator  implements Iterator {

	private Range range;
	private int position;
	
	public RangeRevIterator(Range range) {
		super();
		this.range = range;
		position=range.getMax();
	}
	
	@Override
	public boolean hasNext() {
	if (this.position>=range.getMin())
			
		{ return true; } 
		
		else
			
		{ return false; }
		
	}

	@Override
	public Object next() {
		
		Integer i=position;
		
			this.position--; 
				
		return i;
		
	}

}
