package range;

import java.util.Iterator;

public class RangeIterator implements Iterator<Integer> {

	private Range range;
	private int position;
	
	
	public RangeIterator(Range range) {
		super();
		this.range = range;
		position=range.getMin();
	}

	@Override
	public boolean hasNext() {
	
		if (this.position<=range.getMax())
			
		{ return true; } 
		
		else
			
		{ return false; }
	}

	@Override
	public Integer next() {
		
		Integer i = position;
		
			this.position++; 
				
		return i;
	}
	

   }
