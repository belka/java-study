package range;

import java.util.Iterator;

public class Range implements Iterable<Integer>{
	private int min;
	private int max;
	
	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}
	public Range(int min, int max) {
	
		setMin(min);
		setMax(max);
	}
	@Override
	public Iterator iterator() {
		
		RangeIterator iter = new RangeIterator(this); 
		
		return iter;
	}

}
