import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class FileFolderTest {

	public static void main(String[] args) {
		String folderPath = "c:\\_myfolder";
		File folder = new File(folderPath);
		if (!folder.mkdir())
			  System.out.println("Nain create folder " + folder);
		if (!folder.canWrite())
			  System.out.println("Ain write to folder" + folder);
		
		String filePath = folder + "\\myfile.txt";
		File f = new File(filePath);
		try {
			f.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] strs = {"Kiwi","Apple","Orange","Banana"};
	    try {
		  FileWriter fw=new FileWriter(f);
		  BufferedWriter bw = new BufferedWriter(fw);
			for (String str : strs)
			{
			
				bw.write(str);
				bw.newLine();
			}
			bw.close();	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	    try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			while (true)
			{
			  try {
				
				  String str = br.readLine();
				  if (str==null) 
					        break;
				  System.out.println(str);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}	
				
				
			}
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	    
	/*	if (!f.delete())
			System.out.println("Nain delete file " + f);
		if (!folder.delete())
			  System.out.println("Nain delete folder " + folder);
		*/
	}

}
