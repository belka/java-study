package test;
 
public class HomeStringTest {
 
	public static void main(String[] args) {
 
	String testString =  "NeVagnayFignia";	// default uninit Null
	String revTestString = "aingiFyangaVeN";
	char testChar = 0;	// default uninit = 0
	int startPos = 1;
	int endPos = 10;
 
 
//	if (polyPart(testString,revTestString,startPos,endPos)) {  System.out.println("Est' palindrom"); }
//	else { System.out.println("Net palindroma"); }
 
 
   PrintStringDiapasonColumn (testString,startPos,endPos);
 
//   if (isCharExist(testString, testChar)) { System.out.println("Est' takaya bukva"); } 
//  else { System.out.println("Net takoy bukvi"); }
 
	}
// Proverka palindroma v zadannom diapasone
	public static boolean polyPart(String str, String revstr, int startPos, int endPos)
	{
 
		if(str == null || revstr == null || str.length() == 0 || revstr.length() == 0) { return false; }	
		if (str==null ||  startPos<0 || endPos<0 || startPos>endPos) { return false; }
    	if (str.length()!=revstr.length()) { return false; } 
		if (str.length()-1<startPos || str.length()-1<endPos) { return false; }
 
		str=str.toLowerCase();
		revstr=revstr.toLowerCase();
 
		for(int i = 0,j = revstr.length()-1 ; i <  str.length();i++,j--)
		  {
 
 
			    if((str.charAt(i) != revstr.charAt(j)) & (i>=startPos & i<=endPos)) { return false; }
 
		  }
 
 
	return true;	
	}
 
//Print v stolbik simvolov iz String v zadannom diapasona	
   public static void PrintStringDiapasonColumn (String str, int startPos, int endPos)
   {
 
	if (str==null ||  startPos<0 || endPos<0 || startPos>endPos) { return; }
    if (str.length()-1<startPos || str.length()-1<endPos) { return; } 
 
    for (int i=startPos; i<=endPos; i++)
    {
    	 System.out.println(str.charAt(i));
    }
 
   }
 
 
// Proverka nalichiya bukvi v slove.	
	public static boolean isCharExist (String str, char c)
	{	
	if (str==null ||  str.length()==0 ) { return false; }
 
	for (int i = str.length()-1; i>=0; i--)	
	{
	if (str.charAt(i)==c) { return true; }		
	}
	return false;
	}
 
 
 
 
 
}
 