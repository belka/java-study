package test;

public class StringTest {
public static void main(String[] args) {
    //            01234
    String str = "hello";
    System.out.println(str.length());
    System.out.println(str.charAt(1));
     
    //printStrInColumn("Zemleroika");
    //printStrReverse("Zemleroika");
    String str1 = " Hello  ";
    String str2 = "  olleh   ";
    str1 = str1.trim();
    str2 = str2.trim();
     
    str1 = str1.toLowerCase();
    str2 = str2.toLowerCase();
     
    boolean res = palindrome(str1,str2);
    System.out.println(res);
     
     
     
     
}
public static boolean palindrome(String str1 , String str2)
{
    if(str1 == null || str2 == null || str1.length() == 0
            || str2.length() == 0)
        return false;
    int size1 = str1.length();
    int size2 = str2.length();
    if(size1 != size2)
        return false;
     
    for(int i = 0,j = size2-1 ; i < size1;i++,j--)
    {
        char c1 = str1.charAt(i);
        char c2 = str2.charAt(j);
        if(c1 != c2)
            return false;
    }
    return true;
}
 
 
 
public static void printStrReverse(String str)
{
    int size = str.length();
    for(int i = size-1;i >= 0;i--)
    {
        System.out.println(str.charAt(i));
    }
}
 
public static void printStrInColumn(String str)
{
    int size = str.length();
    for(int i = 0;i < size;i++)
    {
        System.out.println(str.charAt(i));
    }
}
}