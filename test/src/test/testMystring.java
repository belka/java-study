package test;
 
/* Home Work
     all task do with MyString Class.
 * 1) Find the first index of character in the array. 
 * 2) Find count of the character in the array
 * 3) Print array. 
 * 4) Replace all character old by new
 */
 
public class testMystring {
	   public static void main(String[] args) {
 
	       MyString mStr = new MyString("EHLO");
	       MyString mStr2 = new MyString("ayuyy");
	       System.out.println("Length of array: " + mStr.getLength());
	       System.out.println("First Char is: " + mStr.getFirstChar()); 
	       System.out.println("Last Char is: " + mStr.getLastChar());
	       System.out.println("Char on index 2 is: " + mStr.getChar(2));
	  // 1
	       System.out.println("First place index char 'h' is: " + mStr.indexOfChar('h'));
	  // 2   
	       System.out.println("Count of char 'h': " + mStr.charCount('h'));
	 // 4
	       System.out.println("Replaced chars 'h' by 'y' count: " + mStr.replaceChar('h','y'));
	 // 3
	       //mStr.removeChar('y');
	       //System.out.println("Delete char y:" + mStr.toString());
	       char[] chAr = {'a','g'};
	       
	       mStr.removeChars (chAr);
	       
	       System.out.print("Resulted String 1 : ");
	       mStr.printMyString();
	       System.out.println ("Resulted string 2 : " + mStr.toString());
	      // System.out.println(mStr.equals(mStr2));
	       mStr.reverseMyString();
	       System.out.println ("Resulted string 3 : " + mStr.toString());
	      
	       mStr.removeFirstChar('4');	       
		      System.out.print("Resulted String: ");
		      mStr.printMyString();
		   //2
		      mStr.removeLastChar('4');	       
		      System.out.print("Resulted String: ");
		       mStr.printMyString();
		  //3
		       mStr.reverse(0,5);
		       System.out.print("Resulted String: ");
		       mStr.printMyString();
	       
	       
	       
	    		   
	       
	}
	}


