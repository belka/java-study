package test;

/* 1) Print each even word with a capital letters
 * 2) Print last letter in string
 * 3) Print first and last letters in string 
 * 4) Replace first and last letters in string and print it.
 * 
 */


public class Home2StringTest {

	public static void main(String[] args) {
	
	 String teststr="my name is Misha";
	 String delimmiter=" ";
	 char replace = 'X';
	 
	// 1)
	 String resultstr=eachoddwordiscapital(teststr,delimmiter);
	 if (resultstr!=null) { System.out.println(resultstr); }
	 else { System.out.println("Error in arguments"); }
    // 2)
	 printlastletter(teststr);
   // 3)
	 printfirstlastletters (teststr);
  // 4) 
	 printfirstlastletterswithreplace(teststr,replace);
	 
	 
	}

	
	public static void printlastletter  (String s)
	{
		
	if  (s==null) { System.out.println("Error in Arguments : string is null"); return; }	
	
	s=s.trim();
	
	if (!s.isEmpty()) { System.out.println("Last character is : " + s.charAt(s.length()-1)); }
	else { System.out.println("Error in Arguments : string is empty");  return;}
		
	}
	
	public static void printfirstlastletters  (String s)
	{
	
	if  (s==null) { System.out.println("Error in Arguments : string is null"); return; }	
	
	s=s.trim();
	
	if (!s.isEmpty()) { System.out.println("First character is : " + s.charAt(0)+ " Last character is : " + s.charAt(s.length()-1)); }
	
	else { System.out.println("Error in Arguments : string is empty"); return; }
		
	}
	
	
	public static void printfirstlastletterswithreplace  (String s,char c)
	{
	
	if  (s==null) { System.out.println("Error in Arguments : string is null"); return; }	
	
	s=s.trim();
	
	if (s.isEmpty()) { System.out.println("Error in Arguments : string have only spaces"); return; }
	
	  char[] chars = s.toCharArray();
	  chars[0] = c;
	  chars[s.length()-1]=c;
	  
	  System.out.println(String.valueOf(chars));
		
	}
	
	
	
	
		public static String eachoddwordiscapital (String str, String delimitter)
	
	{
	   String[] splitstr = null;
       if (str==null) { return null; }		
	   if (str.length()==0) { return null; }
	   if (delimitter.length()==0) { return null; }
	   str=str.trim();
	   splitstr=str.split(delimitter);
	   
	   str="";  
	   for (int i=0; i<splitstr.length; i++)
	   {
		   
	   splitstr[i]=splitstr[i].trim();  
		   
	  if (evenodd(i)) { splitstr[i]=splitstr[i].toUpperCase(); }
		    
	   if (i==0) { str=splitstr[i]; } else { str=str+" "+splitstr[i];}
	   
	 
	  
	   }
	      
	 
	   
	   
	   
	   return str;
	}
	
	
	
	public static boolean evenodd (int n)  //even or odd.
	{
		
		if ((n & 1) == 0)	{ return true; }

		return false;	
		
	}
	
	
}
