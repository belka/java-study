package com.telran.company.person;

public class Person {

	private String name;
	private String surName;
	private int id;
	private int age;
	
	public Person()
	{
		System.out.println("Default Constructor");
	}
	
	public Person (String name, String surName, int id, int age)
	{
		this.setSurName(surName);
        this.setName(name);
        this.setId(id);
        this.setAge(age);
	
	}
	public void setName (String name)
	{
		this.name=name;
	}
	public void setSurName (String surName)
	{
		this.surName=surName;
	}

	public void setId (int id)
	{
		this.id=id;
	}
	
	public void setAge (int age)
	{
		this.age=age;
	}


	public String getName()
	{
		return name;
	}
	public String getSurName() 
	{
		return surName;
	}

	public int getId ()
	{
		return id;
	}
	
	public int getAge ()
	{
		return age;
	}
	
	@Override
	 public String toString()
	   {
		String str;
		
		str="Name: " + name + " Surname: " + surName + " ID: " + id + " Age: " + age;
	 
		return str;
	 
	   }
	@Override
	public boolean equals (Object obj)
	
	{
		
		if (this==obj) 
		{
			return true;
		}
		if (obj instanceof Person || obj !=null)
		{
		Person p= (Person)obj;
		if (p.id==this.id 
				&& 
			p.age==this.age 
				&&
			p.name.compareTo(this.name)==0 
			    &&
			p.surName.compareTo(this.surName)==0
				)
		return true;
		}
		else 
		{
			
		System.out.println("obj is not instanse of Person object or object is null");	
		}
		return false;
	}
	
}
