package com.telran.company.employee;
import com.telran.company.person.*;

public class Employee  extends Person{

	private String company;
	private double baseSalary;
	
	public Employee(String name, String surName, int id, int age, String company, double baseSalary) {
		super(name, surName, id, age);
		this.company = company;
		this.baseSalary = baseSalary;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public double getBaseSalary() {
		return baseSalary;
	}
	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}

	
	
	
	
	@Override
	public String toString() {
		return "Employee [company=" + company + ", baseSalary=" + baseSalary + ", " + super.toString() + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Employee))
			return false;
		Employee other = (Employee) obj;
		if (Double.doubleToLongBits(baseSalary) != Double.doubleToLongBits(other.baseSalary))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		return true;
	}
	
	public double calculateSalary ()
	{
		return baseSalary;
	}
	
	
}
