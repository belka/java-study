package com.telran.company.employee.wageemployee;

import com.telran.company.employee.Employee;

public class WageEmployee extends Employee  {

	private double wage;
	private double hours;
	
	public double getWage() {
		return wage;
	}
	public void setWage(double wage) {
	
		if (wage<=0) System.out.println("Error: wrong wage");
		else this.wage = wage;
	}
	public double getHours() {
		return hours;
	}
	public void setHours(double hours) {
		
		if (hours<=0) System.out.println("Error: wrong hours");
		else this.hours = hours;
	}
	
	public WageEmployee(String name, String surName, int id, int age, String company, double baseSalary, double wage,
			double hours) {
		super(name, surName, id, age, company, baseSalary);
		this.wage = wage;
		this.hours = hours;
	}
	@Override
	public String toString() {
		return "WageEmployee [wage=" + wage + ", hours=" + hours + ", "  + super.toString() + "]";
	}
	
	@Override
	public double calculateSalary ()
	{
		return super.calculateSalary() + (wage * hours);
	}
	
	
	
	
	
	
}
