package com.telran.company.employee.manager;

import com.telran.company.employee.Employee;

public class Manager extends Employee {

	

	public Manager(String name, String surName, int id, int age, String company, double baseSalary, int grade) {
		
		super(name, surName, id, age, company, baseSalary);
		this.grade = grade;
	}

	private int grade;

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	
	@Override
	public String toString() {
		return "Manager [grade=" + grade + ", " + super.toString() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Manager))
			return false;
		Manager other = (Manager) obj;
		if (grade != other.grade)
			return false;
		return true;
	}

	@Override
	public double calculateSalary() {
		// TODO Auto-generated method stub
		return super.calculateSalary()*grade;
	}
	
	
	
	
}
