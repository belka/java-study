package com.telran.company.employee.sales;

import com.telran.company.employee.Employee;

public class SalesMan extends Employee {

	private double totalSales;
	private double bonus;
	
	public double getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(double totalSales) {
		this.totalSales = totalSales;
	}
	public double getBonus() {
		return bonus;
	}
	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	public SalesMan(String name, String surName, int id, int age, String company, double baseSalary, double totalSales,
			double bonus) {
		super(name, surName, id, age, company, baseSalary);
		this.totalSales = totalSales;
		this.bonus = bonus;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof SalesMan))
			return false;
		SalesMan other = (SalesMan) obj;
		if (Double.doubleToLongBits(bonus) != Double.doubleToLongBits(other.bonus))
			return false;
		if (Double.doubleToLongBits(totalSales) != Double.doubleToLongBits(other.totalSales))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SalesMan [totalSales=" + totalSales + ", bonus=" + bonus + ", " + super.toString() + "]";
	}
	@Override
	public double calculateSalary() {
		// TODO Auto-generated method stub
		return super.calculateSalary() + (totalSales * bonus)/100;
		
	}
	
	
	
	
	
}
