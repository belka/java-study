

import com.telran.company.employee.Employee;
import com.telran.company.employee.wageemployee.WageEmployee;
import com.telran.company.person.*;

public class TestCompany {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    Person pr1 = new Person();
    
    pr1.setName("Haim");
    pr1.setId(1234);
    pr1.setSurName("Mizrahi");
    pr1.setAge(23);
    
    Person pr2 = new Person ("Neta","Barzalay",2345,25);
    Person pr3 = new Person ("Neta","Barzalay",2345,25);
    Object pr4 = null;
    
    
    System.out.println (pr2.toString());
    
    System.out.println (pr2.equals(pr3));
    
    Employee emp1 = new Employee ("Haim","Petrow", 3456, 34, "IBM",2.0 );
    Employee emp2 = new Employee ("Haim","Petrow", 3456, 34, "IBM",1.0 );
    
    System.out.println(emp1.toString());
    System.out.println(emp2.toString());
    
    WageEmployee wemp1 = new WageEmployee ("Haim","Petrow", 3456, 34, "IBM",0.0, 50, 1.5 );
    WageEmployee wemp2 = new WageEmployee ("Haim","Petrow", 3456, 34, "IBM",-0.0, 30, 1.6 );
		
    System.out.println(wemp1.toString());
    System.out.println(wemp2.toString());
    
    calculateAndPrintSalary(emp1);
    
    
	}

	 public static void calculateAndPrintSalary (Employee emp)
	    {
		 System.out.println("Salary of: " + emp.toString());
	    System.out.println("Summ: " + emp.calculateSalary());
	    }
	   	
}
