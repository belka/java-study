package arrayListTest;

import java.util.ArrayList;

public class Company {
   private String name;
   private String address;
    
   private ArrayList<Employee> staff 
                    = new ArrayList<Employee>();
 
public Company(String name, String address) {
    super();
    this.name = name;
    this.address = address;
}
 
public String getName() {
    return name;
}
 
public void setName(String name) {
    this.name = name;
}
 
public String getAddress() {
    return address;
}
 
public void setAddress(String address) {
    this.address = address;
}
public void hireEmployee(Employee emp)
{
    staff.add(emp);
}

public void removeEmployee (int id)
{
	
	staff.remove(id);
}

public boolean findEmployee (int id)
{
   Employee temp = new Employee("", -1, -1 );
	
	temp=staff.get(id);
	
	if (temp.getId()>=0) return true;
	
	return false;
}

public void displayCompany() 
{
    System.out.println("Name : " + name);
    System.out.println("Address : " + address);
    System.out.println("Staff : ");
    for(Employee e: staff)
    {
        System.out.println(e);
    }
     
         
}
    
}