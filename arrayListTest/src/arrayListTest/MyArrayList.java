package arrayListTest;


public class MyArrayList {

	public static void main(String[] args) {
	
		Company company= new Company("California", "Microsoft");
		
		
		Employee emp1 = new Employee("Haim", 1, 35);
		Employee emp2 = new Employee("Sara", 2, 36);
		Employee emp3 = new Employee("Bill",3,40);
		Employee emp4 = new Employee("Pinhas",4,45);
		Employee emp5 = new Employee("Michael",5,50);
		
		
		company.hireEmployee(emp1);
		company.hireEmployee(emp2);
		company.hireEmployee(emp3);
		company.hireEmployee(emp4);
		company.hireEmployee(emp5);
        company.displayCompany();
        System.out.println("");
        System.out.println("Bill was fired");
        System.out.println("");
		company.removeEmployee(3);
		company.displayCompany();
		
		System.out.println("");
        System.out.println("Is Sara working in Company? " + company.findEmployee(2) );
        
		
		
		
	}
}
