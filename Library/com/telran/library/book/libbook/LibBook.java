package com.telran.library.book.libbook;
import com.telran.library.book.Book;

public class LibBook extends Book 
{
	private int catNumber;
	
	
  public  LibBook()
  {
	  super("", "", 0);
	 
	  this.catNumber=0;  
	  
  }

public LibBook(String author, String title, int pages, int catNumber) {
	super(author, title, pages);
	setCatNumber(catNumber);
	}

public int getCatNumber() {
	return catNumber;
}

public void setCatNumber(int catNumber) {
	this.catNumber = catNumber;
}

@Override
public String toString() {
    return super.toString() + " " + "\tCatNumber : " + this.catNumber;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (!super.equals(obj))
		return false;
	if (!(obj instanceof LibBook))
		return false;
	LibBook other = (LibBook) obj;
	if (catNumber != other.catNumber)
		return false;
	return true;
}






}
