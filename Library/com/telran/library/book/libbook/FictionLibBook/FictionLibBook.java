package com.telran.library.book.libbook.FictionLibBook;

import com.telran.library.book.libbook.LibBook;

public class FictionLibBook extends LibBook{

	private String genre;

	
	public FictionLibBook()
	{
		super("", "", 0, 0);
		setGenre("");
	}
	
	
	public FictionLibBook(String author, String title, int pages, int catNumber, String genre) {
		super(author, title, pages, catNumber);
		setGenre(genre);
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	@Override
	public String toString() {
	    return super.toString() + " " + "\tGenre : " + this.genre;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof FictionLibBook))
			return false;
		FictionLibBook other = (FictionLibBook) obj;
		if (genre == null) {
			if (other.genre != null)
				return false;
		} else if (!genre.equals(other.genre))
			return false;
		return true;
	}
	
	
	
}
