package com.telran.library.book;

public class Book {

private String author;
private String title;
private int pages;

public Book()
{
  this.author="";
  this.title="";
  this.pages=0;
}


public Book(String author, String title, int pages) {
	setAuthor(author);
	setTitle(title);
	setPages(pages);
}
public String getAuthor() {
	return author;
}
public void setAuthor(String author) {

	this.author = author;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public int getPages() {
	return pages;
}
public void setPages(int pages) {
    if (pages>0) 
	this.pages = pages;
    else 
    this.pages = 0;
}

@Override
public boolean equals (Object obj)
{
	
if (!(obj instanceof Book) || obj==null) 
{
    return false;	
}
  
Book book = (Book)obj;	
	
if (this.author.equals(book.author) && this.title.equals(book.title) && this.pages==book.pages)

{
	return true;
}

return false;
}


@Override
public String toString() {
	
  if (author!=null && title!=null)
  {
	  return  "Author : " + author
	            + "\t Title : " + title 
	            + "\t Pages : " + pages;
  
  }
  else
  {
	  return "Error, Null string \n";
  }

}






}
