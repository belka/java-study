
public class CinemaHall {
     private int places;
     private String name;
     private String type;
	
     
     
     public void setPlaces(int places) {
		this.places = places;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPlaces() {
		return places;
	}
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public CinemaHall(int places, String name, String type) {
		super();
		this.places = places;
		this.name = name;
		this.type = type;
	}
     
	  public void displayHall ()
	  {
		  System.out.println("Name " + this.getName() + " Type " + this.getType() + " Places " + this.getPlaces() );
	  }
	
}
