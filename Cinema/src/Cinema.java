import java.util.ArrayList;

public class Cinema {

	private String name;
	private String address;
	private ArrayList<Film> films = new ArrayList<Film>();
	private ArrayList<CinemaHall> halls = new ArrayList<CinemaHall>();
	
	
	public Cinema(String name, String address, ArrayList<CinemaHall> halls) {
		super();
		this.name = name;
		this.address = address;
		this.halls = halls;
	}

	public String getName() {
		return name;
	}
	
	public String getAddress() {
		return address;
	}

	public ArrayList<Film> getFilms() {
		return films;
	}
	public void setFilms(ArrayList<Film> films) {
		this.films = films;
	}
	public ArrayList<CinemaHall> getHalls() {
		return halls;
	}
	public void setHalls(ArrayList<CinemaHall> halls) {
		this.halls = halls;
	}
	
	public void addHall (CinemaHall hall)
	{
		halls.add(hall);
	}

	public void closeHall (CinemaHall hall)
	{
				halls.remove(halls.indexOf(hall));
	
	}
	
	public void ChangeHall (CinemaHall old, CinemaHall current)
	{
				halls.set(halls.indexOf(old),current);
			
	}
		
		
	public void addFilm (Film f, CinemaHall h)
	{
		String hallName = h.getName();
		f.setHall(hallName);
		films.add(f);
		
	}
	
	
	public void displayCinema ()
	{
		System.out.println("Cinema Name " + name);
		System.out.println("Halls : ");
		for (CinemaHall h : halls)
		{
			h.displayHall();
		}
		
		for (Film f : films)
		{
			f.displayFilm();
		}
		
	}
	
	
	
	}
	

