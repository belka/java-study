import java.util.ArrayList;

public class Film {

	private String name;
	private String director;
	private double time;
	private String Hall;
	private ArrayList<String> sessions=new ArrayList<String>();


	public void setHall(String hall) {
		Hall = hall;
	}
	
	public String getName() {
		return name;
	}
	public String getDirector() {
		return director;
	}
	public double getTime() {
		return time;
	}
	public String getHall() {
		return Hall;
	}

	public Film(String name, String director, double time) {
		super();
		this.name = name;
		this.director = director;
		this.time = time;
	}

	public void displayFilm()
	{
		System.out.println("Name " + this.getName() + " Director " + this.getDirector() + " Time " + this.getTime());
	    System.out.println("Hall " + this.getHall());
	    System.out.println("Sessions ");
	    
	    for (String s: sessions)
	    {
	    	System.out.print(s + "; " );
	    	
	    }
	    System.out.println("");	
	
	}
	
	public void setSession(String[] session) {
		
		for (String s: session)
		{
	      sessions.add(s);
	      
		}
		
	
		
	}

	
	
	
	
	
	
}
