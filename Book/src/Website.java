
public class Website implements Readable {
      private String url;
      private String owner;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Website(String url, String owner) {
		
		this.url = url;
		this.owner = owner;
	}
	@Override
	public String toString() {
		return "Website [url=" + url + ", owner=" + owner + "]";
	}
	@Override
	public void read() {
		
		
		System.out.println("Website [url=" + url + ", owner=" + owner + "]");
		
		// TODO Auto-generated method stub
		
	}
      
	
	
}
