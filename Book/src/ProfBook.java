
public class ProfBook  extends Book {

	private String proff;
	private int iLevel;
	
	public String getProff() {
		return proff;
	}
	public void setProff(String proff) {
		this.proff = proff;
	}
	public int getiLevel() {
		return iLevel;
	}
	public void setiLevel(int iLevel) {
		this.iLevel = iLevel;
	}
	public ProfBook(String author, String title, int pages, double pricePerPage, String proff, int iLevel) {
		super(author, title, pages, pricePerPage);
		this.proff = proff;
		this.iLevel = iLevel;
	}
	@Override
	public String toString() {
		return "ProfBook [proff=" + proff + ", iLevel=" + iLevel + ", toString()=" + super.toString() + "]";
	}
	@Override
	public double calculatePrice() {
		// TODO Auto-generated method stub
		return super.calculatePrice() * iLevel;
	}
	
	
	
	
	
}
