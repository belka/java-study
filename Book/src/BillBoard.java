
public class BillBoard implements Readable{
    
	private double square; 
	private double pricePerDay;
    
	 public BillBoard(double square, double pricePerDay) {
		super();
		this.square = square;
		this.pricePerDay = pricePerDay;
	}
	public double getPricePerDay() {
		return pricePerDay;
	}
	public void setPricePerDay(double pricePerDay) {
		this.pricePerDay = pricePerDay;
	}
	public double getSquare() {
		return square;
	}
	public void setSquare(double square) {
		this.square = square;
	}
	
	
	@Override
	public String toString() {
		return "BillBoard [square=" + square + ", pricePerDay=" + pricePerDay + "]";
	}
	@Override
	public void read() {
		
		System.out.println("BillBoard [square=" + square + ", pricePerDay=" + pricePerDay + "]");
		// TODO Auto-generated method stub
		
	}
	
     
}
