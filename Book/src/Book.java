
public class Book implements Readable{

	private String author;
	private String title;
	private int pages;
	private double pricePerPage;
	
	
	
	public Book(String author, String title, int pages, double pricePerPage) {
	
		setAuthor(author);
		setTitle(title);
		setPages(pages);
		setPricePerPage(pricePerPage);
	}
	
	
	@Override
	public String toString() {
		return "Book [author=" + author + ", title=" + title + ", pages=" + pages + ", pricePerPage=" + pricePerPage
				+ "]";
	}

	@Override
	public void read() {
		System.out.println ("Book [author=" + author + ", title=" + title + ", pages=" + pages + ", pricePerPage=" + pricePerPage
				+ "]");
	}
	

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public double getPricePerPage() {
		return pricePerPage;
	}
	public void setPricePerPage(double pricePerPage) {
		this.pricePerPage = pricePerPage;
	}
	
	
	
	public double calculatePrice()
	{
		
		
		return this.pricePerPage * this.pages;
	}
	
	
}
