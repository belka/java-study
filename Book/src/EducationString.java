
public class EducationString  extends Book {
	
	private String subject;
	private double subsPercents;
	
	
	
	public EducationString(String author, String title, int pages, double pricePerPage, String subject,
			double subsPercents) {
		super(author, title, pages, pricePerPage);
		this.subject = subject;
		this.subsPercents = subsPercents;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public double getSubsPercents() {
		return subsPercents;
	}
	public void setSubsPercents(double subsPercents) {
		this.subsPercents = subsPercents;
	}

	@Override
	public String toString() {
		return "EducationString [subject=" + subject + ", subsPercents=" + subsPercents + ", toString()="
				+ super.toString() + "]";
	}

	@Override
	public double calculatePrice() {
		
		
		double price = super.calculatePrice();
		double disPrice = (price /100) * subsPercents;  
		// TODO Auto-generated method stub
		return price - disPrice;
		
	}

	

}
