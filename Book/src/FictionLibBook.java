
public class FictionLibBook extends Book {

    private String genre;
    private boolean isBestSeller;
    public static final int BESTSELLER_FACTOR = 2; 
    
    
	public FictionLibBook(String author, String title, int pages, double pricePerPage, String genre,
			boolean isBestSeller) {
		super(author, title, pages, pricePerPage);
		this.genre = genre;
		this.isBestSeller = isBestSeller;
	}
	
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public boolean isBestSeller() {
		return isBestSeller;
	}
	public void setBestSeller(boolean isBestSeller) {
		this.isBestSeller = isBestSeller;
	}
	@Override
	public String toString() {
		return "FictionLibBook [genre=" + genre + ", isBestSeller=" + isBestSeller + ", toString()=" + super.toString()
				+ "]";
	}

	@Override
	public double calculatePrice() {
		
		
		if (this.isBestSeller) 
		{ 	
			return super.calculatePrice() * BESTSELLER_FACTOR; 
		}
		
		return super.calculatePrice();
	}
	
	
		
		
	
	

}
