import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TestFrame extends JFrame {

	 public static JFrame frame = new JFrame("Test frame");
		 
	     public static void createGUI() {
	          
	    	
	 
	           
	          
	     	 String[] columnNames = {
	                 "CarColor",
	                 "Model",
	                 "Year",
	                 "Engine",
	                 "Price"
	       };

	     	  PredicateCarsYearLess pd = new  PredicateCarsYearLess(2012);
	          CarsSaleCort Cort = new CarsSaleCort("Iossi Ha Gadol", "Herzel 25,Haifa",150);
	          Car c1 = new Car ("green",2011 , "toyota", 2.11,50000);
	          Car c2 = new Car ("yellow",2006 , "Carnival", 2.9,21500);
	          Car c3 = new Car ("red",2001 , "suzuki", 1.1,10000);
	   	   	  Car c4 = new Car ("brown",2003 , "mazda", 1.6,13000);
	   	      Car c5 = new Car ("argentum",2005 , "honda", 1.4,15000);
	   	  	  Car c6 = new Car ("baklagan",1987 , "VAZ-2107", 1.5,2000);
	   	  	  Cort.addCar(c1);
	   	  	  Cort.addCar(c2);
	   	  	  Cort.addCar(c3);
	   	  	  Cort.addCar(c4);
	   	  	  Cort.addCar(c5);
	   	  	  Cort.addCar(c6);
	   	  	String[][] atest=new String[Cort.getStock().size()][5];
	  	  	  
	  		for (int i=0; i<Cort.getStock().size();i++)
	  		{
	  		
	  		
	  		Car	test=Cort.getStock().get(i);
	  		if 	(pd.test(test))
	  		{
	  		atest[i][0]=test.getColor();
	  		atest[i][1]=test.getModel();
	  		atest[i][2]=Integer.toString(test.getYear());
	  		atest[i][3]=Double.toString(test.getEngine());
	  		atest[i][4]=Double.toString(test.getPrice());
	  		}
	  		}
	          
	  		
	          
	           
	          JTable table = new JTable(atest, columnNames);
	
	          
	          JScrollPane scrollPane = new JScrollPane(table);
	          frame.getContentPane().add(scrollPane);
	          frame.setPreferredSize(new Dimension(450, 200));
	          frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	          
	          JPanel p2 = new JPanel();
              
	
	          
	          
	          for (int i=0;i<5;i++)
	          {
	        	  JButton t2 = new JButton();
	              
	        	  t2.setActionCommand(Integer.toString(i));
	        	  t2.setText("Sort by " + columnNames[i] + " Asc");
	              t2.addActionListener(new MyActionListener());
	              p2.add(t2);	  
	        	  
	        	  
	          }
	          frame.add(p2, BorderLayout.BEFORE_FIRST_LINE);
	          
	          JPanel p3 = new JPanel();
              
	          
	          for (int i=0;i<5;i++)
	          {
	        	  JButton t2 = new JButton();
	              
	        	  t2.setActionCommand(Integer.toString(i+5));
	        	  t2.setText("Sort by " + columnNames[i] + " Desc");
	              t2.addActionListener(new MyActionListener());
	              p3.add(t2);	  
	        	  
	        	  
	          }
	 	     
	          frame.add(p3, BorderLayout.AFTER_LAST_LINE);
	          frame.pack();
	          frame.setLocationRelativeTo(null);
	          frame.setVisible(true);
	          
	     }
	
	
	     public static void removeGUI()
	     {
	    	 
	     }
	     
	
	public static void main(String[] args) {
	 
	
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
             public void run() {
                 // JFrame.setDefaultLookAndFeelDecorated(true);
                  createGUI();
             }
        }); 
	
		
   }
	}


