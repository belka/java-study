import java.util.Comparator;

public class CarComparatorByModel implements Comparator<Car>
{

	@Override
	public int compare(Car arg0, Car arg1) {
		// TODO Auto-generated method stub
	
		return arg0.getModel().compareToIgnoreCase(arg1.getModel());
	}

	
}
