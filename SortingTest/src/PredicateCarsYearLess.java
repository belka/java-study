import java.util.function.Predicate;

public class PredicateCarsYearLess implements Predicate<Car>{

	private int year;
	
	
	
	public PredicateCarsYearLess(int year) {
		// TODO Auto-generated constructor stub
		setYear(year);
	}



	public int getYear() {
		return year;
	}



	public void setYear(int year) {
		this.year = year;
	}



	@Override
	public boolean test(Car arg0) {
		
		
		if (arg0.getYear()<=getYear()) 
			return true;
		
		
		return false;
	}

	
	
	
}
