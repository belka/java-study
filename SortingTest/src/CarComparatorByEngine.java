import java.util.Comparator;

public class CarComparatorByEngine implements Comparator<Car>{

	@Override
	public int compare(Car arg0, Car arg1) {
		// TODO Auto-generated method stub
		
		if (arg1.getEngine()>arg0.getEngine()) return 1;
		if (arg1.getEngine()<arg0.getEngine()) return -1;	
		if (arg1.getEngine()==arg0.getEngine()) return 0;
	
		return 0;
		//return arg1.getEngine()-arg0.getEngine();
	}

}
