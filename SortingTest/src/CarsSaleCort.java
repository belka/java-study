import java.util.ArrayList;

public class CarsSaleCort {

		private String name;
		private String address;
		private int maxCapacity;
		private ArrayList<Car> stock = null;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public int getMaxCapacity() {
			return maxCapacity;
		}
		public void setMaxCapacity(int maxCapacity) {
			this.maxCapacity = maxCapacity;
		}
		public ArrayList<Car> getStock() {
			return stock;
		}
		public CarsSaleCort(String name, String address, int maxCapacity) {
			super();
			this.name = name;
			this.address = address;
			this.maxCapacity = maxCapacity;
			this.stock = new ArrayList<Car>(maxCapacity);
		}
	
		public void addCar (Car t)
		{
			stock.add(t);
		}
		public void saleCar (Car t)
		{
			stock.remove(stock.indexOf(t));
		}
		public  void displayStock()
		{
			
			System.out.println("Name: " + getName() + " Address: " + getAddress());
					
			for (Car s : stock)
			{
				System.out.println(s.toString());
			}
		    
		    System.out.println("Summary in stock: " + stock.size() + "Cars");
		}
		
	  public void chooseAndDisplayByYearLess (int year)
	  {
		  PredicateCarsYearLess predicate = new PredicateCarsYearLess(year);
		  
		  CarsSaleCort cars= new CarsSaleCort(name, address, maxCapacity);
		  
		  for (Car car: stock)
		  {
			if (predicate.test(car)==true)
				cars.addCar(car);
		  }
		  
		  displayStock();	  
		  
	  }
		
}
