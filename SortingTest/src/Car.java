
public class Car implements Comparable<Car>{

	private String model;
	private int year;
	private String color;
	private double engine;
	private double price;
	
	
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getEngine() {
		return engine;
	}
	public void setEngine(double engine) {
		this.engine = engine;
	}
	public Car(String color, int year, String model, double engine, double price) {
		super();
		this.model = model;
		this.year = year;
		this.color = color;
		this.engine = engine;
		this.price = price;
	}
	@Override
	public String toString() {
		return "Car [model=" + model + ", year=" + year + ", color=" + color + ",Price=" + price + ", engine=" + engine + "]";
	}
	@Override
	public int compareTo(Car arg0) 
	{
		
		
		return this.year - arg0.year;
	}
		
	
}
