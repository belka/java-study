import java.util.Comparator;

public class CarComparatorByColour implements Comparator<Car> {

	@Override
	public int compare(Car arg0, Car arg1) {
		// TODO Auto-generated method stub
		
		return arg0.getColor().compareToIgnoreCase(arg1.getColor());
	}

}
