
public class MyLinkedList {

	private Node head = null;
	private Node tail = null;
	
	public MyLinkedList() {
		
	}

	public Node getHead() {
		return head;
	}

	public Node getTail() {
		return tail;
	}
	
	public void add(Node n)
	{
		if (n.getNext()!=null) 
		{
			System.out.println("Already present");
			return;
		}
		
		if (head == null)
			{
			head = n;
			tail = n;
			}
		else
		{
			tail.setNext(n);
			tail=n;
		}
		
	}
	
	public boolean isEmpty() 
	{
			
	if ( head == null)	return false;
	return true;
	
	}
	
	public void removeHead ()
	{
		if (head == null)
		{
			return;
		}
		
		Node headnew=head;
		head=headnew.getNext();
		
		
	}
	
	
	public void removeTail ()
	{
		if (head == null)
		{
			return;
		}
		
		if (head==tail)
		{
			this.removeHead();
			return;
		}
		
		
		Node curr = head;
		
	while (curr.getNext()!=tail)
	{
		
	  curr = curr.getNext();
	
	}	
       
	 tail = curr;
	 curr = curr.getNext();
	 tail.setNext(null);
	 curr = null;
			 
	 
	}
	
	public void removeNode(int value)
	{
	
		if (this.isExist(value) == false) return;
		
		if (head==null) return;
		if (head==tail)
			
		{
			if (head.getValue()==value) { head = tail = null; }
		    return;
		}
		if (head.getValue() == value)
		{
			this.removeHead();
			return;
		}
		
		
		Node curr = head;
		
		while (curr.getNext().getValue()!=value)
		{
			
		  curr = curr.getNext();
		
		}	
		//System.out.println(curr.setNext(curr.getNext().getNext()));
	   if (curr.getNext()==tail)
	   {
		   tail = curr;
		   tail.setNext(null);
	   }
	      
	   else 
	   {
		   curr.setNext(curr.getNext().getNext());
	   }
		
		
	}
	
	public int elemCount (int i)
	{
		int count=0;
		Node curr = head;
		while (curr != null)
		{
			if (curr.getValue()==i) {count++;}
			curr = curr.getNext();
		}
				
		return count;
	}
	
	
	public void addToHead(Node n)
	{
		if (n == null) 
		{
			System.out.println("Error");
			return;
		}
		
		if (head == null)
		{
			
			head = n;
			tail = n;
			return;
			
		}
		
		n.setNext(head);
		head=n;
	}
	
	
	
	
	
	
	public void addToTail(Node n)
	{
		if (n == null) 
		{
			System.out.println("Error");
			return;
		}
		if (head == null)
		{
			
			head = n;
			tail = n;
			return;
			
		}
		
		
		tail.setNext(n);
		tail=n;
	}
	
	public boolean isExist (int i)
	{
		
		Node curr = head;
		while (curr != null)
		{
			if (curr.getValue()==i) { return true; }
			
			curr = curr.getNext();
		}
		
		return false;
		
	}
	
	
	public void display()
	{
		Node curr = head;
		while (curr != null)
		{
			System.out.println(curr.getValue() + " ");
			curr = curr.getNext();
		}
		
	}

}
