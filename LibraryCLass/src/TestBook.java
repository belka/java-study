
import com.telran.library.book.libbook.*;
import com.telran.library.book.libbook.FictionLibBook.FictionLibBook;
import com.telran.library.book.libbook.educationlibbook.EducationLibBook;


public class TestBook {
	
	
public static void main (String[] args)
{
	
	LibBook lb = new LibBook();
	lb.setAuthor("Lenin V.I.");
	lb.setTitle("How to reorganize us RabKrin");
	lb.setPages(25);
	lb.setCatNumber(1);
	
	LibBook lb1 = new LibBook();
	lb1.setAuthor("Lenin V.I.");
	lb1.setTitle("How to reorganize us RabKrin");
	lb1.setPages(25);
	lb1.setCatNumber(1);
	
	System.out.println(lb.toString());
	System.out.println(lb1.toString());
	System.out.println("Compare  lb vs lb1 :" +lb.equals(lb1));
	
	FictionLibBook flb= new FictionLibBook();
	
	flb.setAuthor("Lenin V.I.");
	flb.setTitle("How to reorganize us RabKrin");
	flb.setPages(25);
	flb.setCatNumber(1);
	flb.setGenre("ScienceFiction");
	
	FictionLibBook flb1 = new FictionLibBook();
	flb1.setAuthor("Lenin V.I.");
	flb1.setTitle("How to reorganize us RabKrin");
	flb1.setPages(25);
	flb1.setCatNumber(1);
	flb1.setGenre("ScienceFiction");
	
	System.out.println(flb.toString());
	System.out.println(flb1.toString());
	System.out.println("Compare  flb vs flb1 :" +flb.equals(flb1));
	
	EducationLibBook elb= new EducationLibBook();
	
	elb.setAuthor("Lenin V.I.");
	elb.setTitle("How to reorganize us RabKrin");
	elb.setPages(25);
	elb.setCatNumber(1);
	elb.setSubject("No Money No Honey");
	
	EducationLibBook elb1 = new EducationLibBook();
	elb1.setAuthor("Lenin V.I.");
	elb1.setTitle("How to reorganize us RabKrin");
	elb1.setPages(25);
	elb1.setCatNumber(1);
	elb1.setSubject("No Money No Honey");
	
	System.out.println(elb.toString());
	System.out.println(elb1.toString());
	System.out.println("Compare  elb vs elb1 :" +elb.equals(lb1));
	
	
}

}
