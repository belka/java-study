package com.telran.library.book.libbook.educationlibbook;

import com.telran.library.book.libbook.LibBook;

public class EducationLibBook extends LibBook{

	private String subject;

	
	
	public EducationLibBook()
	{
		super("", "", 0, 0);
		setSubject("");
	}
	
	
	public EducationLibBook(String author, String title, int pages, int catNumber, String subject) {
		super(author, title, pages, catNumber);
		setSubject(subject);
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	@Override
	public String toString() {
	    return super.toString() + " " + "\tSubject : " + this.subject;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof EducationLibBook))
			return false;
		EducationLibBook other = (EducationLibBook) obj;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}
	
	
	
}
