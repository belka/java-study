import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TestSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        ArrayList<String> list = new ArrayList<String>();
        list.add("Banana");
        list.add("Mivana");
        list.add("Orange");
        list.add("Kiwi");
        list.add("Avokado");
        System.out.println(list);
        
        Collections.sort(list, Collections.reverseOrder());
        //Collections.sort(list);
        System.out.println(list);
        
	}

}
