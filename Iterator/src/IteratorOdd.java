
import java.util.Iterator;
 
public class IteratorOdd implements Iterator {
    private Range range;
    private int position;
    private int lastPosition;
    
     
    public IteratorOdd(Range range)
    {
        this.range = range;
        if (range.getMin()%1==0) 
        	position = range.getMin();
        else 
        	position = range.getMin()+1;
        
        if (range.getMax()%1==0) 
        	lastPosition=range.getMax();
        else
        	lastPosition=range.getMax()-1;
        
    }
       
    @Override
    public boolean hasNext() {
        if(position <= lastPosition)
            return true;
         
        return false;
    }
 
    @Override
    public Object next() {
        
    	int j = position;
        position=position+2;
        
        return j;
    }
 
}
