
public class SalesMan extends Employee{
    private double totalSales;
    private double bonus;
     
    public SalesMan()
    {
         
    }
     
    public SalesMan(String name, String surName,
            int id, 
            double baseSalary, double totalSales,
            double bonus) {
        super(name, surName, id,  baseSalary);
        this.setTotalSales(totalSales);
        this.setBonus(bonus);
    }
 
    public double getTotalSales() {
        return totalSales;
    }
    public void setTotalSales(double totalSales) {
         
    	
    	if (totalSales<0)
    	{
    		System.out.println("Sales can't be a less zero value, using 0 as value");
    		
    		  this.totalSales=0;
    	}
    	
    	else
    	{
    	     this.totalSales = totalSales;
    	}
    }
    public double getBonus() {
        return bonus;
    }
    public void setBonus(double bonus) {
    
    	if (bonus<0)
    	{
    		System.out.println("Bonus can't be a less zero value, using 0 as value");
    	}
    	
    	else 
    	{
    	this.bonus = bonus;
    	}
    }
 

    @Override
	public String toString() {
		return "SalesMan [totalSales=" + totalSales + ", bonus=" + bonus + "] " + super.toString() ;
	}

	@Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
         
        if(this.getClass() != obj.getClass())
            return false;
        SalesMan sm = (SalesMan)obj;
        if(super.equals(sm) && this.totalSales == sm.totalSales
                && this.bonus == sm.bonus)
            return true;
         
         
        return false;
    }
 
    @Override
    public double calculateSalary() {
        return this.getBaseSalary()+ ((totalSales * bonus)/100);
    }
     
     
     
}