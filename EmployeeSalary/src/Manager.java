
public class Manager extends Employee{
   private int grade;
 
   public Manager()
   {
        
   }
      
public Manager(String name, String surName, 
        int id, 
        double baseSalary, int grade) {
    super(name, surName, id,  baseSalary);
    this.setGrade(grade);
}
public int getGrade() {
    return grade;
}
public void setGrade(int grade) {
    if(grade <=0 || grade > 4)
        System.out.println("Error : wrong grade , using 1 as default value");
    else
        this.grade = grade;
}

 
@Override
public String toString() {
	return "Manager [grade=" + grade + "] " + super.toString() ;
}

@Override
public boolean equals(Object obj) {
    if(obj == null)
        return false;
    if(this.getClass() != obj.getClass())
        return false;
     
    Manager mn = (Manager)obj;
    if(super.equals(mn) && this.grade == mn.grade)
        return true;
     
     
    return false;
}
 
@Override
public double calculateSalary() {
    return this.getBaseSalary() * grade;
}
    
    
    
}