
public abstract class Employee {
    private String name;
    private String surname;
    private int id; 
    private double baseSalary;
     
    public Employee()
    {
         
    }
 
	public Employee(String name, String surname, int id, double baseSalary) {
	
		this.setName(name);
		this.setSurname(surname);
		this.setId(id);
		this.setBaseSalary(baseSalary);
	}




	public String getName() {
		return name;
	}

	public void setName(String name) {
	
		if (name.isEmpty() || name==null)
		{
			System.out.println("Can't be null or empty,using default value  - Do");
			this.name="Do";
		}
			
		else
		{
			this.name = name;
		}

		
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {

		if (surname.isEmpty() || surname==null)
		{
			System.out.println("Can't be null or empty,using default value  - John");
			this.surname="John";
		}
			
		else
		{
			this.surname = surname;
		}
		}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		
		if (id<1) 
		{ 
			System.out.println("Error, id<1 using default value 1"); this.id=1; 
		}
		else 
			{
			this.id = id;
			}
	}

	public double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(double baseSalary) {
		if (baseSalary<29.5 ) 
		{ 
			System.out.println("Salary can't be less a minimal, using minimal");
			this.baseSalary=29.5;
		}
		else 
			{
			this.baseSalary = baseSalary;
			}
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", surname=" + surname + ", id=" + id + ", baseSalary=" + baseSalary + "]";
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (Double.doubleToLongBits(baseSalary) != Double.doubleToLongBits(other.baseSalary))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}
    
    public abstract double calculateSalary();
   
   
    
    }
	
	

