import java.util.Comparator;

public class ComparePagesAsc implements Comparator<Book> {

	@Override
	public int compare(Book arg0, Book arg1) {
		
		int compPages=arg0.getPages()-arg1.getPages();
		
		if (compPages>0) 
			return -1;
		else if  (compPages<0) 
		    return 1;
		
		return 0;
	}

}
