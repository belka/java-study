import java.util.Comparator;

public class CompareAllAsc implements Comparator<Book> {

	@Override
	public int compare(Book arg0, Book arg1) {
		
		int compAuthor=arg0.getAuthor().compareToIgnoreCase(arg1.getAuthor());
		int compTitle=arg0.getTitle().compareToIgnoreCase(arg1.getTitle());
		int compPages=arg0.getPages()-arg1.getPages();
		double compPrice=arg0.getPrice()-arg1.getPrice();
		long compISBN= arg0.getISBN()-arg1.getISBN();
		
		
		if (compAuthor>0) 
			return -1;
		else if  (compAuthor<0) 
		    return 1;
		else 
			if (compTitle>0) 
				return -1;
			else if  (compTitle<0) 
			    return 1;
			else
				if (compPages>0) 
					return -1;
				else if  (compPages<0) 
				    return 1;
				else
					if (compPrice>0) 
						return -1;
					else if  (compPrice<0) 
					    return 1;
					else
						if (compISBN>0) 
							return -1;
						else if  (compISBN<0) 
						    return 1;
				return 0;
		
		
		
	}

}
