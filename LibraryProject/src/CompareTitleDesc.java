import java.util.Comparator;

public class CompareTitleDesc implements Comparator<Book> {

	@Override
	public int compare(Book arg0, Book arg1) {
		
		int compTitle=arg0.getTitle().compareToIgnoreCase(arg1.getTitle());
	
		if (compTitle>0) 
			return 1;
		else if  (compTitle<0) 
		    return -1;
		
		
		return 0;
	}

}