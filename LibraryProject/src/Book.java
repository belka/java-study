

public class Book {

	private String author;
	private String title;
	private int pages;
	private double price;
	private long ISBN;
	
	  
	public Book(String author, String title, int pages, double price, long ISBN) {
		super();
		setAuthor(author);
		setTitle(title);
		setPages(pages);
		setPrice(price);
		setISBN(ISBN);
	}
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public long getISBN() {
		return ISBN;
	}
	public void setISBN(long ISBN) {
		this.ISBN = ISBN;
	}
	
	
	

	
}
