import javax.swing.SwingUtilities;

public class LibraryTest {

	public static void main(String[] args) {
		
		Book b1 = new Book("Lenin", "Sobranie", 	1344, 1114, 9785271403514L );
		Book b2 = new Book("Stalin", "Sobranie", 352, 302, 	9785171071905L );
		Book b3 = new Book("Cherchil", "War 39-45", 302, 305, 	9785170981311L );
		Book b4 = new Book("Shekli", "Miracle Coordinates", 1248, 446, 9785170654956L );
		Book b5 = new Book("Garrison", "Bill the hero of Galaxy", 320, 125, 9785170903344L );
		
		Library lib = new Library("MyLibrary", "Hinannit", 25);
		
		lib.addBook(b1);
		lib.addBook(b2);
		lib.addBook(b3);
		lib.addBook(b4);
		lib.addBook(b5);
		
	    SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MyJTable(lib);
            }
        });
		
		
	
	}

}
