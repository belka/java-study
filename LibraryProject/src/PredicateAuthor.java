

import java.util.function.Predicate;
 
public class PredicateAuthor 
                       implements Predicate<Book>{
 
    private String author;
     
    public PredicateAuthor(String author)
    {
        this.author = author;
    }
     
    @Override
    public boolean test(Book arg0) {
       
    	
    	if (arg0.getAuthor().equalsIgnoreCase(author))
    	return true;
        
    	
        return false;
    }

	
 
}
