import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class Library  extends  AbstractTableModel {
  
	
	private static final long serialVersionUID = 1L;
	private String name;
       private String address;
       private int maxBooks;
       private ArrayList<Book> stock = new ArrayList<Book>();
	
       
       public Library(String name, String address, int maxBooks) {
		super();
		setName(name);
		setAddress(address);
		setMaxBooks(maxBooks);
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public int getMaxBooks() {
		return maxBooks;
	}


	public void setMaxBooks(int maxBooks) {
		this.maxBooks = maxBooks;
	}


	public ArrayList<Book> getStock() {
		return stock;
	}


    public boolean addBook (Book bk)
    {
	
    	return  stock.add(bk);
    	
    }
       
    public boolean removeBook (int ISBN)   
    {
    	
    	for (Book bk: stock)
    	{
    	     if (bk.getISBN()==ISBN) 
    	     {
    	    	if (stock.remove(bk)) 
    	    	{
    	    		stock.trimToSize();
    	    		return true;
    	    	}
    	    	else 
    	    		return false; 
    	    	
    	    	 
    	     }
    	}
    	
    	return false;
    	
    }


	@Override
	public int getColumnCount() {
	
		return 5;
	}


	@Override
	public int getRowCount() {
		
		return stock.size();
	}

	  @Override
	    public String getColumnName(int c) {
	        String result = "";
	        switch (c) {
	            case 0:
	                result = "Author";
	                break;
	            case 1:
	                result = "Title";
	                break;
	            case 2:
	                result = "Pages";
	                break;
	            case 3:
	                result = "Price";
	                break;
	            case 4:
	                result = "ISBN";
	                break;    
	                
	        }
	        return result;
	    }
	

	@Override
	public Object getValueAt(int arg0, int arg1) {
		
		Object[] p = stock.toArray();
		
		
	     switch (arg1) {
        case 0:
            return ((Book) p[arg0]).getAuthor();
        case 1:
            return ((Book) p[arg0]).getTitle();
        case 2:
            return ((Book) p[arg0]).getPages();
        case 3:
            return ((Book) p[arg0]).getPrice();
        case 4:
            return ((Book) p[arg0]).getISBN();
        default:
            return "";
	}
       
}  
}

