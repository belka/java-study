import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;

import javax.swing.JTable;

public class MyMouseListener implements MouseListener{

	private Library lib;
	private JTable table;
	private boolean sortAuthor = true;
	private boolean sortTitle = true;
	private boolean sortPages = true;
	private boolean sortPrice = true;
	private boolean sortISBN = true;

	
	
	
	
	public MyMouseListener(Library lib, JTable table) {
		super();
		this.lib = lib;
		this.table = table;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		int col = table.columnAtPoint(arg0.getPoint());
	     switch (col) {
         case 0:
        	 if (sortAuthor)
        	 {
        	 Collections.sort(lib.getStock(),new CompareAuthorDesc());
        	 sortAuthor=false;
        	 }
        	 else 
        	 {
        	Collections.sort(lib.getStock(),new CompareAuthorAsc());
            sortAuthor=true; 
        	 }
             break;
         case 1:
        	 if (sortTitle)
        	 {
        	 Collections.sort(lib.getStock(),new CompareTitleDesc());
        	 sortTitle=false;
        	 }
        	 else
        	 {
             Collections.sort(lib.getStock(),new CompareTitleAsc());
             sortTitle=true;
             }	 
             break;
         case 2:
        	 
        	 if (sortPages)
        	 {
        	 Collections.sort(lib.getStock(),new ComparePagesDesc());
        	 sortPages=false;
        	 }
        	 else
        	 {
        		 Collections.sort(lib.getStock(),new ComparePagesAsc());
            	 sortPages=true;	 
        	 }
        	 
        	 break;
         case 3:
        	 
        	 if (sortPrice)
        	 {
        	 Collections.sort(lib.getStock(),new ComparePriceDesc());
        	 sortPrice=false;
        	 }
        	 else
        	 {
            	 Collections.sort(lib.getStock(),new ComparePriceAsc());
            	 sortPrice=true;
             }
        	 
        	 break;
         case 4:
        	 
        	 if (sortISBN)
        	 {
        	 Collections.sort(lib.getStock(),new CompareISBNDesc());
        	 sortISBN=false;
        	 }
        	 else
        	 {
            	 Collections.sort(lib.getStock(),new CompareISBNAsc());
            	 sortISBN=true;
            }
        		 
        	 break;    
             
     } 
		
		
	     	lib.fireTableDataChanged();
         
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		int col = table.columnAtPoint(arg0.getPoint());
		String name = table.getColumnName(col);
        System.out.println("Press On Header Column to Sort " + col + " " + name);
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
