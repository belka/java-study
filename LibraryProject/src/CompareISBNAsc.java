import java.util.Comparator;

public class CompareISBNAsc implements Comparator<Book> {

	@Override
	public int compare(Book arg0, Book arg1) {

		long compISBN= arg0.getISBN()-arg1.getISBN();
		if (compISBN>0) 
			return -1;
		else if  (compISBN<0) 
		    return 1;
		return 0;
	}

}
