import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

public class MyActionListener implements ActionListener {

	private Library lib;
	private JTextField field;
	private JTextField field1;
	
	private JTable table;
	
	
	
	
	public MyActionListener(Library lib, JTextField field,  JTable table) {
		super();
		this.lib = lib;
		this.field = field;
		this.table = table;
	}

	public MyActionListener(Library lib, JTextField field, JTextField field1, JTable table) {
		super();
		this.lib = lib;
		this.field = field;
		this.field1 = field1;
		this.table = table;
	}



	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		
		
		if (arg0.getActionCommand().equalsIgnoreCase("Select Books by Author"))
		{
		 RowFilter<Object, Object> filter = new RowFilter<Object, Object>() {
		      public boolean include(Entry entry) {
		        String author = (String) entry.getValue(0);
		        
		        if (field.getText().isEmpty()) 
		        return true;
		        return author.equalsIgnoreCase(field.getText());
		      }
		    };
		
		    
		
		    TableRowSorter<Library> sorter = new TableRowSorter<Library>(lib);
		    sorter.setRowFilter(filter);
		    table.setRowSorter(sorter);
		}
	
		
		if (arg0.getActionCommand().equalsIgnoreCase("Select Books by Pages Range"))
		{
			
		 RowFilter<Object, Object> filter = new RowFilter<Object, Object>() {
		      public boolean include(Entry entry) {
		        int pages = (int) entry.getValue(2);
		        int iField = 0;
				int iField1 = 0;
		        
		        if (field.getText().isEmpty() || field1.getText().isEmpty()) 
		        return true;
		        
		        try 
		        {
		        	iField=Integer.parseInt(field.getText());
		            iField1=Integer.parseInt(field1.getText());
		        
		        }
		        catch (Exception e)
		        {
		        	System.out.println("Incorrect symbol into integer fields");
		        	return true;
		        }
		        
		        if (iField>iField1 || iField<0 || iField1<0)
		        return true;	
		        
		        
		        if (pages>=iField && pages<=iField1)
		        return true;
		        
		        return false;
		      }
		    };
		    TableRowSorter<Library> sorter = new TableRowSorter<Library>(lib);
		    sorter.setRowFilter(filter);
		    table.setRowSorter(sorter);
		
		
		}
			if (arg0.getActionCommand().equalsIgnoreCase("Select Books by Price Range"))
			{
				
			 RowFilter<Object, Object> filter = new RowFilter<Object, Object>() {
			      public boolean include(Entry entry) {
			        double price = (double) entry.getValue(3);
			        double iField = 0;
					double iField1 = 0;
			        
			        if (field.getText().isEmpty() || field1.getText().isEmpty()) 
			        return true;
			        
			        
			        try 
			        {
			        
			        	iField=Double.parseDouble(field.getText());
			            iField1=Double.parseDouble(field1.getText());
			        
			        }
			        catch (Exception e)
			        {
			        	System.out.println("Incorrect symbol into integer fields");
			        	return true;
			        }
			        
			        if (iField>iField1 || iField<0 || iField1<0)
			        return true;	
			        
			        
			        if (price>=iField && price<=iField1)
			        return true;
			        
			        return false;
			      }
			    };
			
		    
		    
		
		    TableRowSorter<Library> sorter = new TableRowSorter<Library>(lib);
		    sorter.setRowFilter(filter);
		    table.setRowSorter(sorter);
		}
		
		
		
		    
		    
		    
		    
		 System.out.println(arg0.getActionCommand());
		System.out.println(field.getText());
		System.out.println(field1.getText());
	}

}
