import java.util.Comparator;

public class CompareAuthorDesc implements Comparator<Book> {

	@Override
	public int compare(Book arg0, Book arg1) {
		
		int compAuthor=arg0.getAuthor().compareToIgnoreCase(arg1.getAuthor());
		
		if (compAuthor>0) 
			return 1;
		else if  (compAuthor<0) 
		    return -1;
		
		return 0;
	}

}
