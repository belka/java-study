import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;



public class MyJTable {

	private JTable JLibrary;
	  
	    MyJTable(Library library) {
	        JFrame jfrm = new JFrame("Galaxy " + "\"" + library.getName() + "\"" + " Address : " + library.getAddress());
	        jfrm.getContentPane().setLayout(new FlowLayout());
	        jfrm.setSize(1050,250);
	        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        JLibrary=new JTable(library);
	        JScrollPane jscrlp = new JScrollPane(JLibrary);
	        JLibrary.setPreferredScrollableViewportSize(new Dimension(900, 100));
	        jfrm.getContentPane().add(jscrlp);
	        JLibrary.getTableHeader().addMouseListener(new MyMouseListener(library,JLibrary));
	        
	        
	      
	        
	        JTextField fieldAuthor = new JTextField("input author", 20);
	        JButton btnPress = new JButton("Select Books by Author");
	        btnPress.addActionListener(new MyActionListener(library,fieldAuthor,JLibrary));
	        jfrm.add(fieldAuthor);
	        jfrm.add(btnPress);
	     
	        JTextField fieldPages = new JTextField("from", 20);
	        JTextField fieldPages1 = new JTextField("to", 20);
	        
	        JButton btnPress1 = new JButton("Select Books by Pages Range");
	        btnPress1.addActionListener(new MyActionListener(library,fieldPages,fieldPages1,JLibrary));
	     
	        jfrm.add(fieldPages);
	        jfrm.add(fieldPages1);
	        jfrm.add(btnPress1);
	        
	     
	        JTextField fieldPrice = new JTextField("from", 20);
	        JTextField fieldPrice1 = new JTextField("to", 20);
	        
	        JButton btnPress2 = new JButton("Select Books by Price Range");
	        btnPress2.addActionListener(new MyActionListener(library,fieldPrice,fieldPrice1,JLibrary));
	     
	        jfrm.add(fieldPrice);
	        jfrm.add(fieldPrice1);
	        jfrm.add(btnPress2);
	     
	        
	     
	     
	        jfrm.setVisible(true);
	 
}
}

