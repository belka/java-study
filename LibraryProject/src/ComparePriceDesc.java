import java.util.Comparator;

public class ComparePriceDesc implements Comparator<Book> {

	@Override
	public int compare(Book arg0, Book arg1) {
	
		double compPrice=arg0.getPrice()-arg1.getPrice();
		if (compPrice>0) 
			return 1;
		else if  (compPrice<0) 
		    return -1;
		return 0;
	}

	
	
}
