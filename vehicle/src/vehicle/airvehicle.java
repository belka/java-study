package vehicle;

public abstract  class airvehicle extends vehicle
{

	private double MaxHeight;

	public airvehicle(double avrgSpeed, String model, double maxHeight) {
		super(avrgSpeed, model);
		MaxHeight = maxHeight;
	}


	public double getMaxHeight() {
		return MaxHeight;
	}


	public void setMaxHeight(double maxHeight) {
		MaxHeight = maxHeight;
	}

	@Override
	public String toString() {
		return "airvehicle [MaxHeight=" + MaxHeight + " ]" + super.toString();
	}


	
	
	
	
	
		
	

}
