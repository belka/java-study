package vehicle;

public abstract class vehicle {

	private double avrgSpeed;
	private String model;
	public vehicle(double avrgSpeed, String model) {
		super();
		setAvrgSpeed(avrgSpeed);
		setModel(model);
	}
	public double getAvrgSpeed() {
		return avrgSpeed;
	}
	public void setAvrgSpeed(double avrgSpeed) {
		this.avrgSpeed = avrgSpeed;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	@Override
	public String toString() {
		return "vehicle [avrgSpeed=" + avrgSpeed + ", model=" + model + "]";
	}
	
	public abstract  void  moving();
	
	
		
	
	
	
	
	
}
