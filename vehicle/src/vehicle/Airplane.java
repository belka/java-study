package vehicle;

public class Airplane extends airvehicle {
	
	private boolean isMotor;

	public boolean isMotor() {
		return isMotor;
	}

	public void setMotor(boolean isMotor) {
		this.isMotor = isMotor;
	}

	public Airplane(double avrgSpeed, String model, double maxHeight, boolean isMotor) {
		super(avrgSpeed, model, maxHeight);
		this.isMotor = isMotor;
	}

	@Override
	public String toString() {
		return "Airplane [isMotor=" + isMotor + " ] " + super.toString();
	}

	@Override
	public void moving() {
		System.out.println ("airplane flies in the sky max height: " + this.getMaxHeight());
		
	}
	 
     
	
}
