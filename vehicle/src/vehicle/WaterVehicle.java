package vehicle;

public abstract class WaterVehicle  extends vehicle{

	private String type;

	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public WaterVehicle(double avrgSpeed, String model, String type) {
		super(avrgSpeed, model);
		this.type = type;
	}

	@Override
	public String toString() {
		return "WaterVehicle [type=" + type + "]" + super.toString() ;
	}

	
	
	
}
