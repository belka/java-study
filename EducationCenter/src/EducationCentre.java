import java.util.ArrayList;

public class EducationCentre {
       
	private String name;
    private String address;
    private ArrayList<Course> course = new ArrayList<Course>();
    private ArrayList<ClassRooms> rooms = new ArrayList<ClassRooms>();
	
    
    public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	public EducationCentre(String name, String address) {
		super();
		this.name = name;
		this.address = address;
		
	}
	
	public void addRoom (ClassRooms r)
	{
		
		rooms.add(r);
       	
	}
   
    public void addCourse (Course c, ClassRooms r)
    {
    	
    	c.setRoomNumber(r.getNumber());
    	course.add(c);
    	
    }
    
    public void displayCenter()
    {
    	
    	System.out.println("Name: " + getName() + " Address: " + getAddress());
    	System.out.println("===================================================");
    	for (Course c: course)
    	{
    		c.displayCourse();
    
    		for (ClassRooms r: rooms)
    	{
    		if (r.getNumber()==c.getRoomNumber()) 
    			r.displayRoom();
    
        	
    	}
    		System.out.println("===================================================");	
    		
    		
    	}
    	
    }
    
	
}
