import java.util.ArrayList;

public class ClassRooms {

	private int floor;
	private int maxCapacity;
	private int number;
	
	
	
	public int getFloor() {
		return floor;
	}
	public void setFloor(int floor) {
		this.floor = floor;
	}
	public int getMaxCapacity() {
		return maxCapacity;
	}
	public void setMaxCapacity(int maxCapacity) {

		if (maxCapacity<1) 
		{ 
			System.out.println ("Capacity can't be less a 1");
			maxCapacity=1;
		}
		this.maxCapacity = maxCapacity;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	public ClassRooms(int floor, int maxCapacity, int roomNumber) {
	
		setFloor(floor);
		setMaxCapacity(maxCapacity);
		setNumber(roomNumber);
	}
	
	public void displayRoom()
	{
		System.out.println("Floor: " + getFloor() + " Room Number: " + getNumber() + " Max Capacity: " + getMaxCapacity());
	}
	
	
}
