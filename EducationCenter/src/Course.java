import java.util.ArrayList;

public class Course {

	private String title;
	private int totalHours;
	private String teacherName;
	private int roomNumber;
	private ArrayList<String> schedule = new ArrayList<String>();
	
	
		public Course(String title, int totalHours, String teacherName) {
		
		this.title=title;
		setTotalHours(totalHours);
		setTeacherName(teacherName);
	
		
	}

		
		
		
	public int getRoomNumber() {
			return roomNumber;
		}




		public void setRoomNumber(int roomNumber) {
			this.roomNumber = roomNumber;
		}




	public String getTitle() {
		return title;
	}
	
	public int getTotalHours() {
		return totalHours;
	}
	public void setTotalHours(int totalHours) {
	
		if (totalHours<1) { System.out.println("Total Hours cant be less 1 hour"); totalHours=1; }
		this.totalHours = totalHours;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public ArrayList<String> getSchedule() {
		return schedule;
	}
	
	
	public void setSchedule(String[] sch) {
		
		
		for (String s: sch)
		{
	      schedule.add(s);
	      
		}
		
	}
	
	public void displayCourse()
	{
		
		System.out.println("Curs: " +getTitle() + " Teacher: " + getTeacherName() + " Length Curs: " + getTotalHours());
		
		for (String s: schedule)
		{
			System.out.print(s + "; " );
		}
		System.out.println("");
	}
	
	

}
