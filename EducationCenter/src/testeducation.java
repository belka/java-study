
public class testeducation {

	public static void main(String[] args) {

		ClassRooms rm1=new ClassRooms(5, 20, 1);
		ClassRooms rm2=new ClassRooms(5, 10, 2);
		ClassRooms rm3=new ClassRooms(5, 30, 3);
		ClassRooms rm4=new ClassRooms(5, 2, 4);
		
		Course cs1=new Course("QA", 500, "Igor");
		Course cs2=new Course("C++", 500, "Ivan");
		Course cs3=new Course("JAVA", 500, "Serge");
		Course cs4=new Course("SQL", 500, "Vasiliy");
		
		String[] cs = {"Monday 9.15", "Tuesday 10.30", "Wednesday 16.30", "Friday 20:45" };
		
		cs1.setSchedule(cs);
		cs2.setSchedule(cs);
		cs3.setSchedule(cs);
		cs4.setSchedule(cs);
		
		EducationCentre ed = new EducationCentre("Haifa", "unknown");
		
		ed.addRoom(rm1);
		ed.addRoom(rm2);
		ed.addRoom(rm3);
		ed.addRoom(rm4);
		
		ed.addCourse(cs1, rm1);
		ed.addCourse(cs2, rm3);
		ed.addCourse(cs3, rm4);
		ed.addCourse(cs4, rm2);
		
		ed.displayCenter();
		
	}

}
