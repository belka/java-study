import javax.swing.SwingUtilities;

public class GalaxyTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Galaxy galaxy=new Galaxy("Sol");
		
		Planets p1 = new Planets("Mercury",(long) (3.3*Math.pow(10, 23)),149597870*0.387);
		Planets p2 = new Planets("Venus",(long) (4.87*Math.pow(10, 24)),149597870*0.723);
		Planets p3 = new Planets("Earth",(long) (5.976*Math.pow(10, 24)),149597870);
		Planets p4 = new Planets("Mars",(long) (6.4*Math.pow(10, 23)),149597870*1.52);
		Planets p5 = new Planets("Jupiter",(long) (1.9*Math.pow(10, 27)),149597870*5.2);
		Planets p6 = new Planets("Mars",(long) (1.9*Math.pow(10, 27)),149597870*5.1);
		
		
		
	System.out.println("Adding: " + p1.getName() + " " + galaxy.addPlanet(p1));
	System.out.println("Adding: " + p2.getName() + " " + galaxy.addPlanet(p2));
	System.out.println("Adding: " + p3.getName() + " " + galaxy.addPlanet(p3));
	System.out.println("Adding: " + p1.getName() + " " + galaxy.addPlanet(p1));
	System.out.println("Adding: " + p4.getName() + " " + galaxy.addPlanet(p4));
	System.out.println("Adding: " + p5.getName() + " " + galaxy.addPlanet(p5));
	System.out.println("Adding: " + p6.getName() + " " + galaxy.addPlanet(p6));

	
	
	     SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                new MyJTable(galaxy);
	            }
	        });
		
		
		
	}

}
