import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;


public class MyJTable {

	private JTable JGalaxy;
	  
	    MyJTable(Galaxy galaxy) {
	        //Создаем новый контейнер JFrame
	        JFrame jfrm = new JFrame("Galaxy " + "\"" + galaxy.getName() + "\"" + " Total Planets : " + galaxy.getPlanets().size());
	        //Устанавливаем диспетчер компоновки1
	        jfrm.getContentPane().setLayout(new FlowLayout());
	        //Устанавливаем размер окна
	        jfrm.setSize(900, 200);
	        //Устанавливаем завершение программы при закрытии окна
	        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        //Создаем новую таблицу на основе двумерного массива данных и заголовков
	       
	        Galaxy tModel = galaxy;
	        JGalaxy=new JTable(tModel);
	        
	       
	        //Создаем панель прокрутки и включаем в ее состав нашу таблицу
	        JScrollPane jscrlp = new JScrollPane(JGalaxy);
	        //Устаналиваем размеры прокручиваемой области
	        JGalaxy.setPreferredScrollableViewportSize(new Dimension(900, 100));
	        //Добавляем в контейнер нашу панель прокрути и таблицу вместе с ней
	        
	        jfrm.getContentPane().add(jscrlp);
	        
	        //кнопко
	        JButton btnPress = new JButton("Delete Planet Mars");
	        btnPress.addActionListener(new ActionListener() {
	         //Действие на кнопку.
	        	@Override
				public void actionPerformed(ActionEvent arg0) {
					
	         		//Создаем итератор для поиска и последующего удаления по слову Mars
	        		TreeSet<Planets> p = galaxy.getPlanets();
	         		Iterator<Planets> iterator = p.iterator(); 
	         		
	         		while (iterator.hasNext()) {
	                    Planets test=iterator.next();
	                    if (test.getName().equalsIgnoreCase("Mars")) 
	                    {
	                    System.out.println("Deleting Mars: " + galaxy.deletePlanet(test)); 
	                    tModel.fireTableDataChanged();
	                    jfrm.setTitle("Galaxy " + "\"" + galaxy.getName() + "\"" + " Total Planets : " + galaxy.getPlanets().size());
	         		    break;
	                    }
	         		}
				}
	        });
	        
	        jfrm.add(btnPress);
	        
	        
	        //Отображаем контейнер
	         jfrm.setVisible(true);
	
}
}
