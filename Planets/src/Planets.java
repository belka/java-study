


public class Planets implements Comparable<Planets> {
	
	private String  name;
	private long size;
	private double timeToEarth;
	
	
	
	public Planets(String name, long size, double timeToEarth) {
		super();
		setName(name);
		setSize(size);
		setTimeToEarth(timeToEarth);
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public long getSize() {
		return size;
	}



	public void setSize(long size) {
		this.size = size;
	}



	public double getTimeToEarth() {
		return timeToEarth;
	}



	public void setTimeToEarth(double timeToEarth) {
		this.timeToEarth = timeToEarth;
	}



	
	
	
	
	


	@Override
	public int hashCode() {
		return getName().hashCode() + (int)Double.doubleToLongBits(getSize())+(int)Double.doubleToLongBits(getTimeToEarth());
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planets other = (Planets) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (size != other.size)
			return false;
		if (Double.doubleToLongBits(timeToEarth) != Double.doubleToLongBits(other.timeToEarth))
			return false;
		return true;
	}

	@Override
	public int compareTo(Planets arg0) {
		
	    int testName= getName().compareToIgnoreCase(arg0.getName());	
	    double testTimeToEarth = getTimeToEarth() - arg0.getTimeToEarth();
	    long testSize = getSize() - arg0.getSize();
	    
	   if (testName > 0) 
		   return  1;
	   else if (testName < 0) 
		   return  -1;
	   else
		   if (testSize>0)
			   return 1;
		   else if (testSize<0)
			   return -1;
		   else
			   if (testTimeToEarth>0)
				   return 1;
			   else if (testTimeToEarth<0)
				   return -1;
	   
	    	  
		return 0;
	}



	


	
	
	
	
	
}
