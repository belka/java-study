
public class StringMethods {
	
	public static String reverseInRange (String str,int start,int end)
	{
	
		if (str==null) 
			return null;
		if (str.isEmpty())
			return null;
		if (start<0 || end<0 )
			return null;
		if (end<start  || start>str.length() || end>str.length() )
			return null;
		  
	  
	  char[] ar=str.toCharArray();
	  int i,j;
	  char temp;
	  
	 for (i=start,j=end; i<j; i++,j-- )
	 {
		 
		temp=ar[i];
	
		ar[i]=ar[j];
		
		ar[j]=temp;
	   
	 }
	return new String(ar);	
	}
}
