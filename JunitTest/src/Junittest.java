
public class Junittest {

	
	public static int searchMax (int[] ar)
	{
		
	  if (ar==null || ar.length==0)
		  return -1;
		
	  int max;
	  max = ar[0];
	  
	  
	  
	  for (int i=1; i<ar.length; i++)
	  {
		  
		 if (max<ar[i]) 
			 max=ar[i];
	  }
		return max;
	}
	
	
	public static int searchInt (int[] ar, int value)
	{
		
		if (ar==null || ar.length==0)
						return -1;
		
		for (int i=0; i<ar.length; i++)
		  {
			
			if (ar[i] == value)
				 return i;
			
		  }
		
		return -1;
	}
	
	
	public static int[]  getSubArray  (int[] ar, int start, int end)
	{
		
		if (start>end) 
			 return null;
		if (start<0 || end<0)
			 return null;
		if (start>ar.length || end>ar.length)
			 return null;
		
		int[] arnew = new int[end-start+1]; 
		
		for (int i=start, j=0; i<=end;i++,j++)
		{
			arnew[j]=ar[i];
			System.out.println(arnew[j]);
		}
		
		return arnew;
		
	}

   public static Person getPerson(String name, int id)
   {
	
	   if (name==null || id<=0)
	          return null;
	   
	   return new Person(name,id);
   }

   public static Person[] getPersonArray(String name, int id, int size)
   {
	   
	   if (name==null || id<=0 || size<=0)
	          return null;
	   
	   Person[] pr = new Person[size];
	   
	   
	   for (int i=0;i<size;i++)
	   {
		  pr[i]=getPerson(name,id); 
	   }
		   
	   return pr;
   }
   
}
