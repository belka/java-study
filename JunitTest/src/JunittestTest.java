import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JunittestTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSearchMax() {
		int[] ar = {2,1,4,5,6,7,9,12,6};
		assertTrue(Junittest.searchMax(ar)==12);
        assertFalse(Junittest.searchMax(ar)==0);
		//fail("Not yet implemented");
        ar = null;
        assertTrue(Junittest.searchMax(ar)==-1);
        ar = new int[0];
        assertTrue(Junittest.searchMax(ar)==-1);
	}

	@Test
	public void testSearchInt() {
		int[] ar = {2,1,4,5,6,7,9,12,8};
		
		assertTrue(Junittest.searchInt(ar,12)==7);
		ar = null;
		assertTrue(Junittest.searchInt(ar,12)==-1);
		ar = new int[0];
        assertTrue(Junittest.searchMax(ar)==-1);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testGetSubArray() {
		int[] ar = {2,1,4,5,6,7,9,12,6};
		int[] ar1 = {4,5,6};
		//assertTrue(Junittest.getSubArray(ar,0,4).equals(ar1));
		assertArrayEquals(ar1,Junittest.getSubArray(ar,2,4));
		//fail("Not yet implemented");
	}

	@Test
	public void testGetPerson() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPersonArray() {
		fail("Not yet implemented");
	}

}
