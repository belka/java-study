import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StringMethodsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReverseInRange() {
		
		assertNull(StringMethods.reverseInRange (null,0,0));
		assertNull(StringMethods.reverseInRange ("zemleroyka",-1,2));
		assertNull(StringMethods.reverseInRange ("zemleroyka",6,5));
		assertNull(StringMethods.reverseInRange ("zemleroyka",26,1));
		assertNull(StringMethods.reverseInRange ("zemleroyka",1,27));
		assertNull(StringMethods.reverseInRange ("",1,2));
		assertEquals("mezleroyka",StringMethods.reverseInRange ("zemleroyka",0,2));
		assertEquals("zemleakyor",StringMethods.reverseInRange ("zemleroyka",5,9));
	}

}
