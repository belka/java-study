package com.telran.singer;

public class Bird extends Singer {
	private String birdName;

	public String getbirdName() {
		return birdName;
	}

	public void setbirdName(String birdName) {
	
		if (birdName == null || birdName.isEmpty())
		{
			this.birdName="Unknown";
		}
			
		else 
		{
		this.birdName = birdName;
		}
	}

	public Bird(String songName, String birdName) {
		super(songName);
		setbirdName(birdName);
	}
	@Override
	public void toSing ()
	{
	  
		System.out.print(getbirdName()+ " ");
		super.toSing(); 
		
	}
	

}
