package com.telran.singer;

public class Soul extends Singer {
	private String soulName;

	public String getsoulName() {
		return soulName;
	}

	public void setsoulName(String soulName) {
	
		if (soulName == null || soulName.isEmpty())
		{
			this.soulName="Unknown";
		}
			
		else 
		{
		this.soulName = soulName;
		}
	}

	public Soul(String songName, String soulName) {
		super(songName);
		setsoulName(soulName);
	}
	@Override
	public void toSing ()
	{
	  
		System.out.print(getsoulName()+ " ");
		super.toSing(); 
		
	}
	

}
