package com.telran.singer;

public class Singer {

   private String songName;

public String getSongName() {
	return songName;
}

public void setSongName(String songName) {
	this.songName = songName;
}

public Singer(String songName) {
	
	if (songName==null || songName.isEmpty())
	{
		setSongName("Unknown");
	}
	
	else 
		{
		setSongName(songName);
		}
}

public void toSing ()
{
	
  System.out.println("sings the song " + getSongName());
  
}
   

	
	
	
}
