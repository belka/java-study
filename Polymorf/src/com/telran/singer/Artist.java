package com.telran.singer;

public class Artist extends Singer{

	private String artistName;

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
	
		if (artistName == null || artistName.isEmpty())
		{
			this.artistName="Unknown";
		}
			
		else 
		{
		this.artistName = artistName;
		}
	}

	public Artist(String songName, String artistName) {
		super(songName);
		setArtistName(artistName);
	}
	@Override
	public void toSing ()
	{
	  
		System.out.print(getArtistName()+ " ");
		super.toSing(); 
		
	}
	
	
}
