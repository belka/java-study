import java.io.*;
 
public class CourseMain {
public static void main(String[] args) throws IOException {
    Course course1 = new Course("Software QA testing");
    InputStreamReader is = 
                     new InputStreamReader(System.in);
    BufferedReader br =
                    new BufferedReader(is);
    String answer = null;
    System.out.println("Welcome ! Choose option :");
    while(true) {
    System.out.println("1.Display course");
    System.out.println("2.Add student");
    System.out.println("3.Remove student");
    System.out.println("4.Exit");
     
    answer = br.readLine();
    switch(answer)
    {
    case "1" : course1.displayCourse();break;
    case "4" : System.out.println("Bye ...");return;
    case "2" : addStudents(br,course1);break;
    case "3" : removeStudents(br,course1);break;
    }
    }
}
 
private static void removeStudents(BufferedReader br,
                          Course course1) throws IOException {
    while(true)
    {
        if(course1.getStudentsCount() == 0)
        {
            System.out.println("The course is empty");
            break;
        }
        System.out.println("Please enter id for removing");
        String sId = br.readLine();
        int id = Integer.parseInt(sId);
        course1.removeStudent(id);
        System.out.println("Another student ? yes or no");
        String answer = br.readLine();
        if(answer.equalsIgnoreCase("no"))
            break;
    }
     
}
 
private static void addStudents(BufferedReader br,
                                  Course course1) throws IOException {
    while(true)
    {
        System.out.println("Please enter the name");
        String name = br.readLine();
        System.out.println("Please enter the id");
        String sId = br.readLine();
        int id = Integer.parseInt(sId);
        Student st = new Student(name,id);
        course1.addStudent(st);
    System.out.println("Another student? yes or no");
    String answer = br.readLine();
    if(answer.equalsIgnoreCase("no"))
        break;
    }
}
}