public class Student {
    private String name;
    private int id;
     
    public Student(String name, 
            int id) {
        super();
        this.name = name;
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    @Override
    public int hashCode() {
        return id;
    }
    @Override
    public boolean equals(Object obj) {
        Student s = (Student)obj;
         
        if(this.id == s.id)
            return true;
         
        return false;
    }
    @Override
    public String toString() {
        return "Student [name=" + name + ", id=" + id + "]";
    }
     
     
     
}