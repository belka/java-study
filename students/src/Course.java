import java.util.*;
 
public class Course {
  private String title; 
  private LinkedHashSet<Student> students =
                    new LinkedHashSet<Student>();
public Course(String title) {
    super();
    this.title = title;
}
public void addStudent(Student st)
{
    students.add(st);
}
public void removeStudent(int id)
{
    for(Student st:students)
    {
        if(st.getId() == id) {
            students.remove(st);
            return;
        }
    }
}
public void displayCourse()
{
    System.out.println("Course title : " + title);
    System.out.println("Total students : "
                                  + students.size());
    for(Student st: students)
    {
        System.out.println(st);
    }
}
public int getStudentsCount() {
    return students.size();
}
public void removeAll() {
    students.clear();
}
}