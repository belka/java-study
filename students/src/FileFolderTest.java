import java.io.*;
 
public class FileFolderTest {
public static void main(String[] args) {
    String folderPath = "c:\\_myfolder";
    File folder = new File(folderPath);
    boolean res = folder.mkdir();
    System.out.println("Has folder created : "
                                              + res);
    res = folder.canWrite();
    System.out.println("Can write ? " + res);
    folderPath = "c:\\_myfolder\\_mytest\\tes1";
    File folder1 = new File(folderPath);
    res = folder1.mkdirs();
    System.out.println("Were folders created : "
                                              + res);
    folder1.delete();
     
     
}
}