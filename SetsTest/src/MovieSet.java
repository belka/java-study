import java.util.HashSet;

public class MovieSet {

	public static void main(String[] args) {
	
		HashSet<Movie> set = new HashSet<Movie>();
		Movie f1 = new Movie ("Titanic", 1111, 150);
		Movie f2 = new Movie ("Avatar", 1970, 120);
		Movie f3 = new Movie ("Pulp Fiction", 1991, 110);
		Movie f4 = new Movie ("Kill Bill", 1994, 140);
		Movie f5 = new Movie ("Kill Bill", 1994, 140);

		set.add(f1);
		set.add(f2);
		set.add(f3);
		System.out.println(set.add(f4));
		System.out.println(set.add(f5));
		
		System.out.println(set.toString());
	}

}
