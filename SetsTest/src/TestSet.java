import java.util.*;

public class TestSet {

	public static void main(String[] args) {
	
		String str1 = "Apple";
		String str2 = "Kiwi";
		String str3 = "Orange";
		String str4 = "Banana";
		String str5 = "Grape";
		
		
		int h1 = str1.hashCode();
		int h2 = str2.hashCode();
		int h3 = str3.hashCode();
		int h4 = str4.hashCode();
		int h5 = str5.hashCode();
		
		System.out.println(str1);
		System.out.println(h1);
		
		HashSet<String> set = new HashSet<String>();
		LinkedHashSet<String> set2 = new LinkedHashSet<String>();
		
		
		set.add(str1);
		set.add(str2);
		set.add(str3);
		set.add(str4);
		set.add(str5);
		set.add(str5);
	    System.out.println(set.toString());
		set2.add(str1);
		set2.add(str2);
		set2.add(str3);
		set2.add(str4);
		set2.add(str5);
		set2.add(str5);
	    System.out.println(set2.toString());
	    TreeSet<Integer> seti = new TreeSet<Integer>();
	    
	    seti.add(2);
	    seti.add(9);
	    seti.add(4);
	    seti.add(0);
	    seti.add(8);
	    System.out.println(seti.toString());
	    System.out.println(seti.add(3));
	}

}
