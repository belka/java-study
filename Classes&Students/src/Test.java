
public class Test {

	public static void main(String[] args) {
		
		
		Subject sb1 = new Subject("Algebra", "Room 1");
		Subject sb2 = new Subject("Geometry", "Room 2");
		Subject sb3 = new Subject("History", "Room 3");
		Subject sb4 = new Subject("English", "Room 4");
		Subject sb5 = new Subject("Physis", "Room 5");
		Students st1 = new Students("Default Curs", "Main Building");
		
		st1.addSubject(sb1);
		st1.addSubject(sb2);
		st1.addSubject(sb3);
		st1.addSubject(sb4);
		st1.addSubject(sb5);
		
		st1.Display();
		
	}

}
