import java.util.ArrayList;

public class Students {

	 private String name;
	 private String location;
	 private ArrayList<Subject> listSubj = new ArrayList<Subject>();
	
	 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		
		if (name.isEmpty() || name ==  null)
			this.name = "Default Name";
		else 
			this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		
		if (location.isEmpty() || location ==  null)
			this.location = "Default Location";
		else	
			this.location = location;
	}
	public ArrayList<Subject> getListSubj() {
		return listSubj;
	}
	
	
	public Students(String name, String location) {
		super();
		setName(name);
		setLocation(location);
	}
	
	public boolean addSubject (Subject subj)
	{
	
	    return this.listSubj.add(subj);	
	}
	
	public void Display()
	{
		System.out.println("Name: " + this.name);
		System.out.println("Location: " + this.location);
		
		for (Subject sub : this.listSubj)
		{
			
			System.out.println(sub.toString());
			
			
		}
		
		
	}
	
}
