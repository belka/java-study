
public class Subject {

	private String subject;
	private String location;

	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		
		if (subject.isEmpty() || subject==null)
		   this.subject = "Default subject";
		else
			this.subject = subject;
	}
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
	
		if (location.isEmpty() || location==null)
		
			this.location="Default Location";
		else
			
			this.location = location;
	}

	public Subject(String subject, String location) {
		super();
		setSubject(subject);
		setLocation(location);
	}

	@Override
	public String toString() {
		
		return "Subject: \t" + subject + " \t Location: " + location ;
	
	}
	
	
}
